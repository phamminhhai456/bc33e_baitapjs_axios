var dssv = [];
var BASE_URL = "https://62f8b7553eab3503d1da15e2.mockapi.io";

function turnOnLoading() {
  document.querySelector("#loading").style.display = "flex";
}

function turnOffLoading() {
  document.querySelector("#loading").style.display = "none";
}

var renderTable = function (list) {
  var contentHtml = "";
  list.forEach(function (item) {
    var trContent = `
    <tr>
      <td>${item.ma}</td>
      <td>${item.ten}</td>
      <td>${item.email}</td>
      <td><img src="${item.hinhAnh}" style="width:40px; height:40px" alt=""/></td>
      <td><button class="btn btn-danger" onclick="xoaSinhVien(${item.ma})">Xóa</button></td>
      <td><button class="btn btn-warning" onclick="suaSV(${item.ma})">Sửa</button></td>
    </tr>`;
    contentHtml += trContent;
  });
  document.querySelector("#tbodySinhVien").innerHTML = contentHtml;
};

var renderDssvService = function () {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      dssv = res.data;
      renderTable(dssv);
    })
    .catch(function (err) {
      turnOffLoading();
    });
};

renderDssvService();

function xoaSinhVien(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      renderDssvService();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}

function themSV() {
  var dataForm = layThongTinTuForm();
  turnOnLoading;
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      turnOffLoading();
      renderDssvService();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}

function suaSV(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
    });
}

function capNhatSV() {
  var id = document.querySelector("#txtMaSV").value;
  var dataForm = layThongTinTuForm();
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "PUT",
    data: dataForm,
  })
    .then(function (res) {
      turnOffLoading();
      renderDssvService();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}
