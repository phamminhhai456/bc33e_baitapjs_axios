function getEle(tag) {
  return document.querySelector(tag);
}
function layThongTinTuForm() {
  var ten = getEle("#txtTenSV").value;
  var email = getEle("#txtEmail").value;
  var hinhAnh = getEle("#txtImg").value;

  return {
    ten: ten,
    email: email,
    hinhAnh: hinhAnh,
  };
}

function showThongTinLenForm(data) {
  getEle("#txtMaSV").value = data.ma;
  getEle("#txtTenSV").value = data.ten;
  getEle("#txtEmail").value = data.email;
  getEle("#txtImg").value = data.hinhAnh;
}
